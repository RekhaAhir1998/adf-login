import { Injectable } from '@angular/core';
import { AlfrescoApiService, AuthenticationService } from '@alfresco/adf-core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GetapidataService {

  constructor(private alfAuthuthService: AuthenticationService,
    private router: Router,
    private apiService: AlfrescoApiService) { }

  postWebscript(URL, data) {

    if (!this.alfAuthuthService.isEcmLoggedIn()) {
      this.router.navigate(['/auth/login']);
      return;
    }
    return new Promise((resolve, reject) => {
      this.apiService.getInstance().webScript.executeWebScript('POST', URL, null, null, null, data)
        .then(function (res) {
          return resolve(res);
        }).catch(error => {
          return reject(error);
        });
    });
  }

  postWebscriptWithoutAuth(URL, data) {

    return new Promise((resolve, reject) => {
      this.apiService.getInstance().webScript.executeWebScript('POST', URL, null, null, null, data)
        .then(function (res) {
          return resolve(res);
        }).catch(error => {
          return reject(error);
        });
    });
  }
  getWebScript(URL) {

    if (!this.alfAuthuthService.isEcmLoggedIn()) {
      this.router.navigate(['/auth/login']);
      return;
    }

    return new Promise((resolve, reject) => {
      this.apiService.getInstance().webScript.executeWebScript('GET', URL)
        .then(function (res) {
          return resolve(res);
        }).catch(error => {
          return reject(error);
        });
    });
  }

  deleteWebScript(URL) {

    if (!this.alfAuthuthService.isEcmLoggedIn()) {
      this.router.navigate(['/auth/login']);
      return;
    }

    return new Promise((resolve, reject) => {
      this.apiService.getInstance().webScript.executeWebScript('DELETE', URL)
        .then(function (res) {
          return resolve(res);
        }).catch(error => {
          return reject(error);
        });
    });
  }
}
