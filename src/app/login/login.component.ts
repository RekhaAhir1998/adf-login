import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {


  loginForm: FormGroup;
  myAngularxQrCode = 'Your QR code data string';
  
  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    this.loginForm = this.fb.group({
      uname: ['',[Validators.required, Validators.name]],
        password: ["", Validators.required],
        otp: ["", Validators.required]
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      alert('valid user')
    }
    else {
      alert('enter valid data')
    }

  }
}
